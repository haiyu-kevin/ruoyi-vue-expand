package com.ruoyi.base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ruoyi.base.domain.BaseExportTemp;

/**
 * ureport2模板管理Mapper接口
 * 
 * @author ruoyi
 * @date 2022-01-21
 */
public interface BaseExportTempMapper 
{
    /**
     * 查询ureport2模板管理
     * 
     * @param id ureport2模板管理主键
     * @return ureport2模板管理
     */
    public BaseExportTemp selectBaseExportTempById(Long id);

    /**
     * 查询ureport2模板管理列表
     * 
     * @param baseExportTemp ureport2模板管理
     * @return ureport2模板管理集合
     */
    public List<BaseExportTemp> selectBaseExportTempList(BaseExportTemp baseExportTemp);

    /**
     * 新增ureport2模板管理
     * 
     * @param baseExportTemp ureport2模板管理
     * @return 结果
     */
    public int insertBaseExportTemp(BaseExportTemp baseExportTemp);

    /**
     * 修改ureport2模板管理
     * 
     * @param baseExportTemp ureport2模板管理
     * @return 结果
     */
    public int updateBaseExportTemp(BaseExportTemp baseExportTemp);

    /**
     * 删除ureport2模板管理
     * 
     * @param id ureport2模板管理主键
     * @return 结果
     */
    public int deleteBaseExportTempById(Long id);

    /**
     * 批量删除ureport2模板管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBaseExportTempByIds(Long[] ids);
    /**
     * 删除ureport2模板管理
     *
     * @param id ureport2模板管理ID
     * @return 结果
     */
    public int setDeleteById(@Param("id")Long id,@Param("operId")Long operId,@Param("operName")String operName);

    /**
     * 批量删除ureport2模板管理---逻辑删除
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int setDeleteByIds(@Param("ids")Long[] ids,@Param("operId")Long operId,@Param("operName")String operName);

    /**
     * 根据code查询参数
     * @param code
     * @return
     */
    public BaseExportTemp selectParamByCode(@Param("code") String code);

    /**
     * 根据模板的存放路径查询唯一的模板信息
     * @param fullPath
     * @return
     */
    public BaseExportTemp selectSignleByFullPath(@Param("fullPath") String fullPath);
}
