import request from '@/utils/request'

// 查询ureport2模板管理列表
export function listBaseExportTemp(query) {
  return request({
    url: '/base/baseExportTemp/list',
    method: 'get',
    params: query
  })
}

// 查询ureport2模板管理详细
export function getBaseExportTemp(id) {
  return request({
    url: '/base/baseExportTemp/' + id,
    method: 'get'
  })
}

// 新增ureport2模板管理
export function addBaseExportTemp(data) {
  return request({
    url: '/base/baseExportTemp',
    method: 'post',
    data: data
  })
}

// 修改ureport2模板管理
export function updateBaseExportTemp(data) {
  return request({
    url: '/base/baseExportTemp',
    method: 'put',
    data: data
  })
}

// 删除ureport2模板管理
export function delBaseExportTemp(id) {
  return request({
    url: '/base/baseExportTemp/' + id,
    method: 'delete'
  })
}
// 新增或编辑模板
export function addTemp(query) {
  return request({
    url: '/base/baseExportTemp/addTemp',
    method: 'get',
    params: query
  })
}
// 新增或编辑模板
export function editTemp(id) {
  return request({
    url: '/base/baseExportTemp/editTemp/'+id,
    method: 'get'
  })
}
// 预览模板
export function previwTemp(id) {
  return request({
    url: '/base/baseExportTemp/previwTemp/'+id,
    method: 'get'
  })
}
// 业务预览模板
export function businessPreviwTemp(query) {
  return request({
    url: '/base/baseExportTemp/businessPreviwTemp',
    method: 'get',
    params:query
  })
}
// 业务---导出Excel
export function businessExportExcel(query) {
  return request({
    url: '/base/baseExportTemp/businessExportExcel',
    method: 'get',
    params:query
  })
}