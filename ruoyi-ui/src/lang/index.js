import Vue from "vue";
//国际化支持
import VueI18n from 'vue-i18n'
import ElementEnLocale from 'element-ui/lib/locale/lang/en'
import ElementZhLocale from 'element-ui/lib/locale/lang/zh-CN'
import enLoLocale from './en'
import zhLoLocale from './zh'

Vue.use(VueI18n)

const i18n = new VueI18n({
    locale: localStorage.lang || 'zh', // set locale
    messages:{
        en:{
            ...enLoLocale,
            ...ElementEnLocale
        },
        zh:{
            ...zhLoLocale,
            ...ElementZhLocale
        }
    } // set locale messages
});

export default i18n;