package ${packageName}.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.domain.ResponseData;
import java.util.List;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import ${packageName}.domain.${ClassName};
import ${packageName}.service.I${ClassName}Service;
import com.ruoyi.common.utils.poi.ExcelUtil;
#if($table.crud || $table.sub)
import com.ruoyi.common.core.page.TableDataInfo;
#elseif($table.tree)
#end

/**
 * ${functionName}Controller
 * 
 * @author ${author}
 * @date ${datetime}
 */
@Api(value = "${functionName}",tags = "${functionName}")
@ApiSupport(author = "${author}",order = 10)
@RestController
@RequestMapping("/${moduleName}/${businessName}")
public class ${ClassName}Controller extends BaseController
{
    @Autowired
    private I${ClassName}Service ${className}Service;

    /**
     * 查询${functionName}列表
     */
    @ApiOperation(value = "查询${functionName}列表",notes = "查询${functionName}列表")
    @ApiOperationSupport(order = 10)
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:list')")
    @GetMapping("/list")
#if($table.crud || $table.sub)
    public TableDataInfo<${ClassName}> list(${ClassName} ${className})
    {
        startPage();
        List<${ClassName}> list = ${className}Service.select${ClassName}List(${className});
        return getDataTable(list);
    }
#elseif($table.tree)
    public AjaxResult list(${ClassName} ${className})
    {
        List<${ClassName}> list = ${className}Service.select${ClassName}List(${className});
        return AjaxResult.success(list);
    }
#end

    /**
     * 导出${functionName}列表
     */
    @ApiOperation(value = "导出${functionName}列表",notes = "导出${functionName}列表")
    @ApiOperationSupport(order = 20)
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:export')")
    @Log(title = "${functionName}", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ${ClassName} ${className})
    {
        List<${ClassName}> list = ${className}Service.select${ClassName}List(${className});
        ExcelUtil<${ClassName}> util = new ExcelUtil<${ClassName}>(${ClassName}.class);
        util.exportExcel(response, list, "${functionName}数据");
    }

    /**
     * 获取${functionName}详细信息
     */
    @ApiOperation(value = "获取${functionName}详细信息",notes = "获取${functionName}详细信息")
    @ApiOperationSupport(order = 30)
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:query')")
    @GetMapping(value = "/{${pkColumn.javaField}}")
    public ResponseData<${ClassName}> getInfo(@PathVariable("${pkColumn.javaField}") ${pkColumn.javaType} ${pkColumn.javaField})
    {
        return ResponseData.success(${className}Service.select${ClassName}By${pkColumn.capJavaField}(${pkColumn.javaField}));
    }

    /**
     * 新增${functionName}
     */
    @ApiOperation(value = "新增${functionName}",notes = "新增${functionName}")
    @ApiOperationSupport(order = 40,ignoreParameters = {"${className}.${pkColumn.javaField}"})
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:add')")
    @Log(title = "${functionName}", businessType = BusinessType.INSERT)
    @PostMapping
    @RepeatSubmit
    public ResponseData add(@RequestBody @Valid @ApiParam(required=true) ${ClassName} ${className})
    {
        int i = ${className}Service.insert${ClassName}(${className});
        if(i>0){
            //返回主键
            return ResponseData.success(${className}.getId());
        }else{
            return ResponseData.error();
        }
    }

    /**
     * 修改${functionName}
     */
    @ApiOperation(value = "修改${functionName}",notes = "修改${functionName}")
    @ApiOperationSupport(order = 50)
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:edit')")
    @Log(title = "${functionName}", businessType = BusinessType.UPDATE)
    @PutMapping
    @RepeatSubmit
    public ResponseData edit(@RequestBody @Valid @ApiParam(required=true) ${ClassName} ${className})
    {
        int i = ${className}Service.update${ClassName}(${className});
        if(i>0){
            //返回主键
            return ResponseData.success(${className}.getId());
        }else{
            return ResponseData.error();
        }
    }

    /**
     * 删除${functionName}
     */
    @ApiOperation(value = "删除${functionName}",notes = "删除${functionName}")
    @ApiOperationSupport(order = 60)
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:remove')")
    @Log(title = "${functionName}", businessType = BusinessType.DELETE)
	@DeleteMapping("/{${pkColumn.javaField}s}")
    public ResponseData remove(@PathVariable ${pkColumn.javaType}[] ${pkColumn.javaField}s)
    {
        //return toResult(${className}Service.delete${ClassName}ByIds(${pkColumn.javaField}s));
        //逻辑删除
        return toResult(${className}Service.setDeleteByIds(${pkColumn.javaField}s));
    }

    /**
     * 查询${functionName}列表---不进行分页
     */
    @ApiOperation(value = "查询${functionName}列表---不进行分页",notes = "查询${functionName}列表---不进行分页")
    @ApiOperationSupport(order = 70)
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:list')")
    @PostMapping("/getDataList")
    public ResponseData<List<${ClassName}>> getDataList(@RequestBody ${ClassName} ${className})
    {
        List<${ClassName}> list = ${className}Service.select${ClassName}List(${className});
        return ResponseData.success(list);
    }
}
