package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 转换为能够被swagger2
 * @param <T>
 */
@ApiModel(value = "返回菜单数据子类对象")
@Data
public class ResponseDataMenu<T> extends ResponseData {
    @ApiModelProperty(value = "当前用户选中菜单主键集合")
    private List<Long> checkedKeys;
    @ApiModelProperty(value = "下拉树结构的菜单信息")
    private List<TreeSelect> menus;

    public static <T> ResponseDataMenu successMenu(T data) {
        ResponseDataMenu<T> success = successMenu();
        success.setData(data);
        return success;
    }

    public static <T> ResponseDataMenu successMenu(T data, List<Long> checkedKeys, List<TreeSelect> menus) {
        ResponseDataMenu<T> success = successMenu();
        success.setData(data);
        success.setCheckedKeys(checkedKeys);
        success.setMenus(menus);
        return success;
    }

    public static <T> ResponseDataMenu successMenu(List<Long> checkedKeys, List<TreeSelect> menus) {
        ResponseDataMenu<T> success = successMenu();
        success.setCheckedKeys(checkedKeys);
        success.setMenus(menus);
        return success;
    }

    public static <T> ResponseDataMenu successMenu() {
        ResponseDataMenu<T> success = new ResponseDataMenu<T>();
        success.setCode(HttpStatus.SUCCESS);
        success.setMsg("请求成功");
        success.setSuccess(true);
        success.setData(null);
        return success;
    }
}
