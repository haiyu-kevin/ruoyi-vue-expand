package com.ruoyi.common.core.domain;

import com.ruoyi.common.constant.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 转换为能够被swagger2
 * @param <T>
 */
@ApiModel(value = "返回部门数据子类对象")
@Data
public class ResponseDataDept<T> extends ResponseData {
    @ApiModelProperty(value = "当前用户选中的部门主键集合")
    private List<Long> checkedKeys;
    @ApiModelProperty(value = "下拉树结构的部门信息")
    private List<TreeSelect> depts;

    public static <T> ResponseDataDept successDept(T data) {
        ResponseDataDept<T> success = successDept();
        success.setData(data);
        return success;
    }

    public static <T> ResponseDataDept successDept(T data, List<Long> checkedKeys, List<TreeSelect> depts) {
        ResponseDataDept<T> success = successDept();
        success.setData(data);
        success.setCheckedKeys(checkedKeys);
        success.setDepts(depts);
        return success;
    }

    public static <T> ResponseDataDept successDept(List<Long> checkedKeys, List<TreeSelect> depts) {
        ResponseDataDept<T> success = successDept();
        success.setCheckedKeys(checkedKeys);
        success.setDepts(depts);
        return success;
    }

    public static <T> ResponseDataDept successDept() {
        ResponseDataDept<T> success = new ResponseDataDept<T>();
        success.setCode(HttpStatus.SUCCESS);
        success.setMsg("请求成功");
        success.setSuccess(true);
        success.setData(null);
        return success;
    }
}
